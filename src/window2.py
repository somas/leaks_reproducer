# window.py
#
# Copyright 2023 Manuel Genovés
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later
import gc

from gi.repository import Adw
from gi.repository import Gtk, Gio, GLib

@Gtk.Template(resource_path='/org/gnome/Example/window2.ui')
class Test2Window2(Adw.ApplicationWindow):
    __gtype_name__ = 'Test2Window2'

    label = Gtk.Template.Child()

    def __init__(self, parent, **kwargs):
        super().__init__(application=Gio.Application.get_default())
        self.a = parent.weak_ref(self.on_drop)

        GLib.timeout_add(10000, self.print_debug)

    def on_drop(self, args):
        print("drop")

    def print_debug(self):
        print(gc.get_referrers(self.a()))
        return True
